#pragma once
#include "RigidBody.h"
#include "Gizmos.h"
class Box :
	public RigidBody
{
public:
	Box(glm::vec2 position, glm::vec2 extents, glm::vec2 velocity, float mass,  glm::vec4 colour) :
		RigidBody(BOX, position, velocity, 0, mass)
	{
		m_extents = extents;
		m_colour = colour;
		m_moment = 1.0f/12.0f * m_mass * 2* extents.x * 2*extents.y;
	}
	~Box();

	void fixedUpdate(glm::vec2 gravity, float timeStep);
	void makeGizmo();
	float getWidth() { return m_extents.x * 2; }
	float getHeight() { return m_extents.y * 2; }
	float getMoment() { return m_moment; }

	glm::vec2 getLocalX() { return m_localX; }
	glm::vec2 getLocalY() const { return m_localY; }
	glm::vec2 getExtents() const { return m_extents; }

	bool checkBoxCorners(const Box& box, glm::vec2& contact, int& numContacts, float &pen, glm::vec2& edgeNormal);

	void setCosmetic(bool isCosmetic);

private:
	bool m_isCosmetic;
	
	glm::vec2 m_extents;
	glm::vec4 m_colour;

		//store the local x,y axes of the box based on it's angle of rotation

};

