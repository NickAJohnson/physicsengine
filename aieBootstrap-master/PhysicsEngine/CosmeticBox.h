#pragma once
#include "Box.h"
class CosmeticBox :
	public Box
{
public:

	CosmeticBox(glm::vec2 position, glm::vec2 extents, glm::vec2 velocity, glm::vec4 colour) : Box(position, extents, velocity, INT_MAX, colour)
	{
		setShapeID(COSMETIC_BOX);
	}
	~CosmeticBox();


	void fixedUpdate(glm::vec2 gravity, float timeStep);

private:

};


