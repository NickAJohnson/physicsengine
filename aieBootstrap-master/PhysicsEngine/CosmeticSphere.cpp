#include "CosmeticSphere.h"

CosmeticSphere::~CosmeticSphere()
{
}

void CosmeticSphere::fixedUpdate(glm::vec2 gravity, float timeStep)
{
	if (m_isRandom)
	{
		if (m_frameCounter > 0)
		{
			m_frameCounter--;
		}
		else
		{
			int velX = (rand() % 101 - 50) * m_spasticity;
			int velY = (rand() % 101 - 50) * m_spasticity;
			//m_velocity = glm::vec2(velX, velY);
			applyForce(glm::vec2((float)velX, (float)velY), getPosition());
			m_frameCounter = 10;
		}

	}
	RigidBody::fixedUpdate(gravity, timeStep);
}

void CosmeticSphere::makeGizmo()
{
	aie::Gizmos::add2DCircle(m_position, m_radius, 16, m_colour, nullptr);
}
