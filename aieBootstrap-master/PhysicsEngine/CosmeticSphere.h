#pragma once
#include "Sphere.h"
#include <stdlib.h>

class CosmeticSphere :
	public Sphere
{
public:
	CosmeticSphere(glm::vec2 position, glm::vec2 velocity, float radius, glm::vec4 colour) : Sphere(position, velocity, 1.0f, radius, colour)
	{
		setShapeID(COSMETIC_SPHERE);
		m_isRandom = false;
		m_spasticity = .1f;
		m_frameCounter = 10;
	}
	~CosmeticSphere();

	void fixedUpdate(glm::vec2 gravity, float timeStep);
	void makeGizmo();
	void setRandom(bool isRandom) { m_isRandom = isRandom; }
	void setSpasticity(float spasticity) { m_spasticity = spasticity; }

private:
	bool m_isRandom;
	float m_spasticity;
	int m_frameCounter;

};

