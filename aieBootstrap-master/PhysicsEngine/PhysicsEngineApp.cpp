#include "PhysicsEngineApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"

PhysicsEngineApp::PhysicsEngineApp() {

}

PhysicsEngineApp::~PhysicsEngineApp() {

}


bool PhysicsEngineApp::startup() {
	// increase the 2d line count to maximize the number of objects we can draw
	aie::Gizmos::create(255U, 255U, 65535U, 65535U);

	backgroundBottom = new aie::Texture("../bin/textures/OceanBackgroundBottom.png");
	backgroundMiddle = new aie::Texture("../bin/textures/OceanBackground.png");
	backgroundTop = new aie::Texture("../bin/textures/OceanBackgroundSurface.png");
	rock = new aie::Texture("../bin/textures/Rock.png");
	victory = new aie::Texture("../bin/textures/Victory.png");

	m_2dRenderer = new aie::Renderer2D();

	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = new aie::Font("../bin/font/consolas.ttf", 32);

	srand(time(nullptr));
	m_physicsScene = new PhysicsScene(getWindowWidth(), getWindowHeight());
	//Active Scenes
	//basicShapeTesting();
	//art();

	buffer = new char[30];
	strcpy_s(buffer, 30, "Hello");


	startUpMenu();
	return true;
}


void PhysicsEngineApp::shutdown() {

	delete m_physicsScene;
	delete m_font;
	delete m_2dRenderer;
}

void PhysicsEngineApp::update(float deltaTime) {

	if (deltaTime > .1f) { deltaTime = .1f; }
	// input example
	aie::Input* input = aie::Input::getInstance();

	aie::Gizmos::clear();

	m_physicsScene->update(deltaTime);
	m_physicsScene->updateGizmos();

	if (displayScenes)
	{
		//navigate scene selector
		if (input->wasKeyPressed(aie::INPUT_KEY_DOWN))
		{
			if (sceneSelector < totalScenes)
			{
				sceneSelector++;
			}
		}
		if (input->wasKeyPressed(aie::INPUT_KEY_UP))
		{
			if (sceneSelector > 1)
			{
				sceneSelector--;
			}
		}
		if (input->isKeyDown(aie::INPUT_KEY_ENTER))
		{
			m_physicsScene->clearScene();
			switch (sceneSelector)
			{
				case 1:
				{
					art();
					break;
				}
				case 2:
				{
					basicShapeTesting();
					break;
				}
				case 3:
				{
					springTesting();
					break;
				}
				case 4:
				{
					letterFall();
					break;
				}
				case 5:
				{
					softBodies();
					break;
				}
				default :
				{
					break;
				}
			}

			displayScenes = false;
		}
	}


	if (letterFallActive)
	{
		letterFallUpdate(deltaTime);
	}
	else if (softBodiesActive)
	{
		softBodiesUpdate(deltaTime);
	}

	// exit the application
	if (input->wasKeyPressed(aie::INPUT_KEY_ESCAPE))
		if (!displayScenes)
		{
			m_physicsScene->clearScene();
			displayScenes = true;
			underwaterRocksList.clear();
			letterFallActive = false;
			softBodiesActive = false;
			winner = false;
		}
		else
		{
			quit();
		}
}

void PhysicsEngineApp::draw() {

	// wipe the screen to the background colour
	clearScreen();
	// begin drawing sprites
	m_2dRenderer->begin();
	m_2dRenderer->setCameraPos(0, 0);
	aie::Input* input = aie::Input::getInstance();
	// draw your stuff here!
	static float aspectRatio = 16 / 9.0f;
	if (softBodiesActive)
	{
		float depth = 0;
		m_2dRenderer->drawSprite(backgroundBottom, 1280, -120,0,0,0,depth);
		m_2dRenderer->drawSprite(backgroundMiddle, 1280, 815, 0, 0, 0, depth);
		m_2dRenderer->drawSprite(backgroundMiddle, 1280, 1750, 0, 0, 0, depth);
		m_2dRenderer->drawSprite(backgroundMiddle, 1280, 2685, 0, 0, 0, depth);
		m_2dRenderer->drawSprite(backgroundMiddle, 1280, 3620, 0, 0, 0, depth);
		m_2dRenderer->drawSprite(backgroundMiddle, 1280, 4555, 0, 0, 0, depth);
		m_2dRenderer->drawSprite(backgroundMiddle, 1280, 5490, 0, 0, 0, depth);
		m_2dRenderer->drawSprite(backgroundTop, 1280, 6405, 0, 0, 0, depth);

		if (winner)
		{
			m_2dRenderer->drawSprite(victory, thePlayer->getCamX() + 640, thePlayer->getCamY() + 360);
		}
		for (int i = 0; i < underwaterRocksList.size(); i++)
		{
			m_2dRenderer->drawSprite(rock, convertXGizmoToBoot(underwaterRocksList[i]->getPosition().x), convertYGizmoToBoot(underwaterRocksList[i]->getPosition().y));
		}
		m_2dRenderer->setCameraPos(thePlayer->getCamX(), thePlayer->getCamY());

		
		aie::Gizmos::draw2D(glm::ortho<float>(-100 + thePlayer->getPosition().x, 100 + thePlayer->getPosition().x,
			(-100 / aspectRatio) + thePlayer->getPosition().y, (100 / aspectRatio) + thePlayer->getPosition().y, -1.0f, 1.0f));
	//TODO add the pictures

	}
	else
	{
		aie::Gizmos::draw2D(glm::ortho<float>(-100, 100, -100 / aspectRatio, 100 / aspectRatio, -1.0f, 1.0f));
	}

	if (input->isKeyDown(aie::INPUT_KEY_LEFT_CONTROL))
	{
		m_physicsScene->displayEnergy(m_font, m_2dRenderer);
	}
	if (displayScenes)
	{
		float increment = 1.0f / totalScenes;
		m_2dRenderer->drawText(m_font, "Art1", getWindowWidth() / 2.0f, (1.0f - ((1.0f - (totalScenes + 1.0f)/2.0f) * increment)) *getWindowHeight() / 2.0f );
		m_2dRenderer->drawText(m_font, "BasicShapeTesting", getWindowWidth() / 2.0f, (1.0f - ((2.0f - (totalScenes + 1.0f)/2.0f) * increment)) *getWindowHeight() / 2.0f );
		m_2dRenderer->drawText(m_font, "SpringTesting", getWindowWidth() / 2.0f, (1.0f - ((3.0f - (totalScenes + 1.0f)/2.0f) * increment)) *getWindowHeight() / 2.0f );
		m_2dRenderer->drawText(m_font, "LetterFalling", getWindowWidth() / 2.0f, (1.0f - ((4.0f - (totalScenes + 1.0f) / 2.0f) * increment)) *getWindowHeight() / 2.0f);
		m_2dRenderer->drawText(m_font, "SoftBodies", getWindowWidth() / 2.0f, (1.0f - ((5.0f - (totalScenes + 1.0f) / 2.0f) * increment)) *getWindowHeight() / 2.0f);

		m_2dRenderer->drawCircle(getWindowWidth() / 2.0f - 30.0f, 15.0f + (1.0f - ((sceneSelector - (totalScenes + 1.0f) / 2.0f) * increment)) *getWindowHeight() / 2.0f, 15, 0.0f);


	}

	// output some text, uses the last used colour
	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);


	// done drawing sprites
	m_2dRenderer->end();
}

void PhysicsEngineApp::startUpMenu()
{
	displayScenes = true;
	sceneSelector = 1;
}

void PhysicsEngineApp::basicShapeTesting()
{

	m_physicsScene->setGravity(glm::vec2(0, -98));
	m_physicsScene->setTimeStep(0.01f);

	Sphere* sphere1, *sphere2;
	sphere1 = new Sphere(glm::vec2(20, 30), glm::vec2(0, 0), .170, 6, glm::vec4(1, 0, 0, 1));
	sphere2 = new Sphere(glm::vec2(-20, -30), glm::vec2(0, 200), .160, 6, glm::vec4(1, 0, 0, 1));
	m_physicsScene->addActor(sphere1);
	m_physicsScene->addActor(sphere2);

	Plane* plane1, *plane2, *plane3, *plane4;
	plane1 = new Plane(glm::vec2(0, 1), -50);
	plane2 = new Plane(glm::vec2(0, 1), 50);
	plane3 = new Plane(glm::vec2(1, 0), -50);
	plane4 = new Plane(glm::vec2(1, 0), 50);
	m_physicsScene->addActor(plane1);
	m_physicsScene->addActor(plane2);
	m_physicsScene->addActor(plane3);
	m_physicsScene->addActor(plane4);

	Box* box1, *box2;
	box1 = new Box(glm::vec2(0, 0), glm::vec2(50, 6), glm::vec2(0, 0), 2.0f, glm::vec4(0, 1, 0, 1));
	box2 = new Box(glm::vec2(40, 40), glm::vec2(5, 5), glm::vec2(0, 0), 2.0f, glm::vec4(0, 1, 0, 1));
	box1->setKinematic(true);
	m_physicsScene->addActor(box1);
	m_physicsScene->addActor(box2);

	return;
}

void PhysicsEngineApp::springTesting()
{
	m_physicsScene->setGravity(glm::vec2(0, -98));
	m_physicsScene->setTimeStep(0.01f);

	Sphere* sphere1, *sphere2;
	sphere1 = new Sphere(glm::vec2(-20, 5), glm::vec2(100, 0), .170, 6, glm::vec4(1, 0, 0, 1));
	sphere2 = new Sphere(glm::vec2(-20, 0), glm::vec2(0, 0), .160, 6, glm::vec4(1, 0, 0, 1));
	sphere1->setKinematic(true);
	m_physicsScene->addActor(sphere1);
	m_physicsScene->addActor(sphere2);

	Spring* spring1;
	spring1 = new Spring(sphere1, sphere2, 20, 10);
	m_physicsScene->addActor(spring1);
	//	Spring(RigidBody* body1, RigidBody* body2, float restLength, float springCoefficient, 
	//	float damping = 0.1f, glm::vec2 contact1 = glm::vec2(0, 0), glm::vec2 contact2 = glm::vec2(0, 0)) : PhysicsObject(JOINT)


	Plane* plane1, *plane2, *plane3, *plane4;
	plane1 = new Plane(glm::vec2(0, 1), -50);
	plane2 = new Plane(glm::vec2(0, 1), 50);
	plane3 = new Plane(glm::vec2(1, 0), -50);
	plane4 = new Plane(glm::vec2(1, 0), 50);
	m_physicsScene->addActor(plane1);
	m_physicsScene->addActor(plane2);
	m_physicsScene->addActor(plane3);
	m_physicsScene->addActor(plane4);



	return;
}

void PhysicsEngineApp::art()
{ 

	bool yeahItIs = true;
	m_physicsScene->setGravity(glm::vec2(0, -98));
	m_physicsScene->setTimeStep(0.01f);

	glm::vec4 skinColour(.9, .7, .5, 1);
	glm::vec4 hairColour(.6, .2, .1, 1);


	//Head
	CosmeticSphere* cSphere3, *cSphere4;
	cSphere3 = new CosmeticSphere(glm::vec2(0, 15), glm::vec2(0, 0), 30.0f, skinColour);
	cSphere4 = new CosmeticSphere(glm::vec2(0, -10), glm::vec2(0, 0), 27.0f, skinColour);
	cSphere3->setKinematic(true);
	cSphere4->setKinematic(true);
	m_physicsScene->addActor(cSphere3);
	m_physicsScene->addActor(cSphere4);

	//Mouth
	CosmeticSphere* cSphere5;
	Box* box20;
	cSphere5 = new CosmeticSphere(glm::vec2(10, -10), glm::vec2(0, 0), 5.6f, glm::vec4(0, 0, 0, 1));
	box20 = new Box(glm::vec2(10, 0), glm::vec2(5.6, 10), glm::vec2(0, 0), 2.0f, skinColour);
	box20->setKinematic(true);
	box20->setCosmetic(true);
	cSphere5->setKinematic(true);
	m_physicsScene->addActor(cSphere5);
	m_physicsScene->addActor(box20);

	//Glasses
	Box* box1, *box2, *box3, *box4, *box5, *box6, *box7;
	box1 = new Box(glm::vec2(13, 10), glm::vec2(10, 8), glm::vec2(0, 0), 2.0f, glm::vec4(0, 0, 0, 1));
	box2 = new Box(glm::vec2(-13, 10), glm::vec2(10, 8), glm::vec2(0, 0), 2.0f, glm::vec4(0, 0, 0, 1));
	box3 = new Box(glm::vec2(13, 9.7f), glm::vec2(9.15f, 7), glm::vec2(0, 0), 2.0f, skinColour);
	box4 = new Box(glm::vec2(-13, 9.7f), glm::vec2(9.15f, 7), glm::vec2(0, 0), 2.0f, skinColour);
	box5 = new Box(glm::vec2(0, 13), glm::vec2(4, .5f), glm::vec2(0, 0), 2.0f, glm::vec4(0, 0, 0, 1));
	box6 = new Box(glm::vec2(-26, 15), glm::vec2(4, .5f), glm::vec2(0, 0), 2.0f, glm::vec4(0, 0, 0, 1));
	box7 = new Box(glm::vec2(26, 15), glm::vec2(4, .5f), glm::vec2(0, 0), 2.0f, glm::vec4(0, 0, 0, 1));
	box1->setCosmetic(true);
	box2->setCosmetic(true);
	box3->setCosmetic(true);
	box4->setCosmetic(true);
	box5->setCosmetic(true);
	box6->setCosmetic(true);
	box7->setCosmetic(true);
	box1->setKinematic(true);
	box2->setKinematic(true);
	box3->setKinematic(true);
	box4->setKinematic(true);
	box5->setKinematic(true);
	box6->setKinematic(true);
	box7->setKinematic(true);
	m_physicsScene->addActor(box1);
	m_physicsScene->addActor(box2);
	m_physicsScene->addActor(box3);
	m_physicsScene->addActor(box4);
	m_physicsScene->addActor(box5);
	m_physicsScene->addActor(box6);
	m_physicsScene->addActor(box7);


	//Eyes
	Sphere* sphere1, *sphere2;
	sphere1 = new Sphere(glm::vec2(13, 10), glm::vec2(0, 0), 1, 6, glm::vec4(1, 1, 1, 1));
	sphere2 = new Sphere(glm::vec2(-13, 10), glm::vec2(0, 0), 1, 6, glm::vec4(1, 1, 1, 1));
	sphere1->setKinematic(true);
	sphere2->setKinematic(true);
	m_physicsScene->addActor(sphere1);
	m_physicsScene->addActor(sphere2);

	CosmeticSphere* cSphere1, *cSphere2;
	cSphere1 = new CosmeticSphere(glm::vec2(13, 10), glm::vec2(0, 0), 2.6f, glm::vec4(0, 0, 0, 1));
	cSphere2 = new CosmeticSphere(glm::vec2(-13, 10), glm::vec2(0, 0), 2.6f, glm::vec4(0, 0, 0, 1));
	cSphere1->setRandom(true);
	cSphere2->setRandom(true);
	m_physicsScene->addActor(cSphere1);
	m_physicsScene->addActor(cSphere2);

	String* string1, *string2;
	string1 = new String(sphere1, cSphere1, 4, 1000);
	string2 = new String(sphere2, cSphere2, 4, 1000);
	m_physicsScene->addActor(string1);
	m_physicsScene->addActor(string2);

	//Hair
	float springStrength = 10.0f;

	Box* box8, *box9, *box10, *box11, *box12, *box13, *box14, *box15, *box16, *box17, *box18, *box19;
	Spring* spring1, *spring2, *spring3, *spring4, *spring5, *spring6, *spring7, *spring8, *spring9, *spring10;
	box8 = new Box(glm::vec2(15, 37), glm::vec2(14, 1), glm::vec2(0, 0), 2.0f, hairColour); // Base
	box9 = new Box(glm::vec2(-15, 37), glm::vec2(14, 1), glm::vec2(0, 0), 2.0f, hairColour);
	box10 = new Box(glm::vec2(13, 35), glm::vec2(9.15f, 1), glm::vec2(0, 0), 2.0f, hairColour); //1st connection
	box11 = new Box(glm::vec2(-13, 35), glm::vec2(9.15f, 1), glm::vec2(0, 0), 2.0f, hairColour);
	spring1 = new Spring(box8, box10, 2.0f, springStrength * 2, .3f, glm::vec2 (13,0), glm::vec2(-2,0));
	spring2 = new Spring(box9, box11, 2.0f, springStrength * 2, .3f, glm::vec2(-13, 0), glm::vec2(2, 0));
	box12 = new Box(glm::vec2(15, 31), glm::vec2(10, 1.0f), glm::vec2(0, 0), 2.0f, hairColour); //2nd connection
	box13 = new Box(glm::vec2(-15, 31), glm::vec2(10, 1.0f), glm::vec2(0, 0), 2.0f, hairColour);
	spring3 = new Spring(box10, box12, 2.0f, springStrength, .3f, glm::vec2(3, 0), glm::vec2(-2, 0));
	spring4 = new Spring(box11, box13, 2.0f, springStrength, .3f, glm::vec2(-3, 0), glm::vec2(2, 0));
	box14 = new Box(glm::vec2(17, 27), glm::vec2(10, 1.0f), glm::vec2(0, 0), 2.0f, hairColour); //3rd connection
	box15 = new Box(glm::vec2(-17, 27), glm::vec2(10, 1.0f), glm::vec2(0, 0), 2.0f, hairColour);
	spring5 = new Spring(box12, box14, 2.0f, springStrength, .3f, glm::vec2(3, 0), glm::vec2(-2, 0));
	spring6 = new Spring(box13, box15, 2.0f, springStrength, .3f, glm::vec2(-3, 0), glm::vec2(2, 0));
	box16 = new Box(glm::vec2(19, 24), glm::vec2(10, 1.0f), glm::vec2(0, 0), 2.0f, hairColour); //4th connection
	box17 = new Box(glm::vec2(-19, 24), glm::vec2(10, 1.0f), glm::vec2(0, 0), 2.0f, hairColour);
	spring7 = new Spring(box14, box16, 2.0f, springStrength, .3f, glm::vec2(3, 0), glm::vec2(-2, 0));
	spring8 = new Spring(box15, box17, 2.0f, springStrength, .3f, glm::vec2(-3, 0), glm::vec2(2, 0));
	box18 = new Box(glm::vec2(21, 21), glm::vec2(10, 1.0f), glm::vec2(0, 0), 2.0f, hairColour); //5th connection
	box19 = new Box(glm::vec2(-21, 21), glm::vec2(10, 1.0f), glm::vec2(0, 0), 2.0f, hairColour);
	spring9 = new Spring(box16, box18, 2.0f, springStrength, .3f, glm::vec2(3, 0), glm::vec2(-2, 0));
	spring10 = new Spring(box17, box19, 2.0f, springStrength, .3f, glm::vec2(-3, 0), glm::vec2(2, 0));

	spring1->isThisMikesHair(yeahItIs);
	spring2->isThisMikesHair(yeahItIs);
	spring3->isThisMikesHair(yeahItIs);
	spring4->isThisMikesHair(yeahItIs);
	spring5->isThisMikesHair(yeahItIs);
	spring6->isThisMikesHair(yeahItIs);
	spring7->isThisMikesHair(yeahItIs);
	spring8->isThisMikesHair(yeahItIs);
	spring9->isThisMikesHair(yeahItIs);
	spring10->isThisMikesHair(yeahItIs);

	box8->setKinematic(true);
	box9->setKinematic(true);


	m_physicsScene->addActor(box8);
	m_physicsScene->addActor(box9);
	m_physicsScene->addActor(box10);
	m_physicsScene->addActor(box11);
	m_physicsScene->addActor(box12);
	m_physicsScene->addActor(box13);
	m_physicsScene->addActor(box14);
	m_physicsScene->addActor(box15);
	m_physicsScene->addActor(box16);
	m_physicsScene->addActor(box17);
	m_physicsScene->addActor(box18);
	m_physicsScene->addActor(box19);

	m_physicsScene->addActor(spring1);
	m_physicsScene->addActor(spring2);
	m_physicsScene->addActor(spring3);
	m_physicsScene->addActor(spring4);
	m_physicsScene->addActor(spring5);
	m_physicsScene->addActor(spring6);
	m_physicsScene->addActor(spring7);
	m_physicsScene->addActor(spring8);
	m_physicsScene->addActor(spring9);
	m_physicsScene->addActor(spring10);

}

void PhysicsEngineApp::letterFall()
{
	letterFallActive = true;
	m_physicsScene->setGravity(glm::vec2(0.01f, -98.0f));
	shapeBuilder.addToString("Hi");

	Plane *plane;
	plane = new Plane(glm::vec2(0, 1), -60);
	m_physicsScene->addActor(plane);
	
}

void PhysicsEngineApp::letterFallUpdate(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();

	ImGui::Begin("Letterfall");
	if (ImGui::InputText("Text", buffer , 30))
	{
		
	}
	if (ImGui::Button("Go!"))
	{
		shapeBuilder.addToString(buffer);
	}

	ImGui::End();

	if (input->isKeyDown(aie::INPUT_KEY_LEFT))
	{
		m_physicsScene->setGravity(m_physicsScene->getGravity() + glm::vec2(-1, 0));
	}

	if (input->isKeyDown(aie::INPUT_KEY_RIGHT))
	{
		m_physicsScene->setGravity(m_physicsScene->getGravity() + glm::vec2(1, 0));
	}

	shapeBuilder.update(m_physicsScene, deltaTime);
	//check if array empty

	//print a letter
}

void PhysicsEngineApp::softBodies()
{
	softBodiesActive = true;
	m_physicsScene->setGravity(glm::vec2(0, 0));
	
	thePlayer = new SoftBodyPlayer(m_physicsScene, 8, 5.0f, 200000, glm::vec2(0, 0), 1.01f, 2.0f, false);
	Plane* plane, *plane2, *plane3, *plane4;
	plane = new Plane(glm::vec2(0, 1), -softBodyBoundarySize);
	plane2 = new Plane(glm::vec2(1, 0), softBodyBoundarySize);
	plane3 = new Plane(glm::vec2(1, 0), -softBodyBoundarySize);
	m_physicsScene->addActor(plane);
	m_physicsScene->addActor(plane2);
	m_physicsScene->addActor(plane3);

	Box* box1;
	box1 = new Box(glm::vec2(160, 300), glm::vec2(36, 4), glm::vec2(0, 0), (float)INT_MAX, glm::vec4(1, 1, 1, 1));
	box1->setKinematic(true);
	m_physicsScene->addActor(box1);
	underwaterRocksList.push_back(box1);
	box1 = new Box(glm::vec2(-160, 500), glm::vec2(36, 4), glm::vec2(0, 0), (float)INT_MAX, glm::vec4(1, 1, 1, 1));
	box1->setKinematic(true);
	m_physicsScene->addActor(box1);
	underwaterRocksList.push_back(box1);
	box1 = new Box(glm::vec2(0, 160), glm::vec2(36, 4), glm::vec2(0, 0), (float)INT_MAX, glm::vec4(1, 1, 1, 1));
	box1->setKinematic(true);
	m_physicsScene->addActor(box1);
	underwaterRocksList.push_back(box1);
	box1 = new Box(glm::vec2(-40, 440), glm::vec2(36, 4), glm::vec2(0, 0), (float)INT_MAX, glm::vec4(1, 1, 1, 1));
	box1->setKinematic(true);
	m_physicsScene->addActor(box1);
	underwaterRocksList.push_back(box1);
	box1 = new Box(glm::vec2(60, 620), glm::vec2(36, 4), glm::vec2(0, 0), (float)INT_MAX, glm::vec4(1, 1, 1, 1));
	box1->setKinematic(true);
	m_physicsScene->addActor(box1);
	underwaterRocksList.push_back(box1);
	box1 = new Box(glm::vec2(30, 560), glm::vec2(36, 4), glm::vec2(0, 0), (float)INT_MAX, glm::vec4(1, 1, 1, 1));
	box1->setKinematic(true);
	m_physicsScene->addActor(box1);
	underwaterRocksList.push_back(box1);
	box1 = new Box(glm::vec2(50, 380), glm::vec2(36, 4), glm::vec2(0, 0), (float)INT_MAX, glm::vec4(1, 1, 1, 1));
	box1->setKinematic(true);
	m_physicsScene->addActor(box1);
	underwaterRocksList.push_back(box1);
	box1 = new Box(glm::vec2(-110, 290), glm::vec2(36, 4), glm::vec2(0, 0), (float)INT_MAX, glm::vec4(1, 1, 1, 1));
	box1->setKinematic(true);
	m_physicsScene->addActor(box1);
	underwaterRocksList.push_back(box1);
	box1 = new Box(glm::vec2(-60, 780), glm::vec2(36, 4), glm::vec2(0, 0), (float)INT_MAX, glm::vec4(1, 1, 1, 1));
	box1->setKinematic(true);
	m_physicsScene->addActor(box1);
	underwaterRocksList.push_back(box1);
	box1 = new Box(glm::vec2(60, 700), glm::vec2(36, 4), glm::vec2(0, 0), (float)INT_MAX, glm::vec4(1, 1, 1, 1));
	box1->setKinematic(true);
	m_physicsScene->addActor(box1);
	underwaterRocksList.push_back(box1);

	for (int i = 0; i < totalSoftBodies; i++)
	{
		int layers = rand() % 6 + 3;
		Direction direction = Direction(rand() % 4);
		glm::vec2 position;
		switch (direction)
		{
			case 0:
			{
				position = glm::vec2(-softBodyBoundarySize + layers * 10, (rand() % (2*((int)softBodyBoundarySize - layers * 10))) - ((int)softBodyBoundarySize - layers * 10));
				break;
			}
			case 1:
			{
				position = glm::vec2(softBodyBoundarySize - layers * 10, (rand() % (2 * ((int)softBodyBoundarySize - layers * 10))) - ((int)softBodyBoundarySize - layers * 10));
				break;
			}
			case 2:
			{
				position = glm::vec2((rand() % (2 * ((int)softBodyBoundarySize - layers * 10))) - ((int)softBodyBoundarySize - layers * 10), softBodyBoundarySize - layers * 10);
				break;
			}
			case 3:
			{
				position = glm::vec2((rand() % (2 * ((int)softBodyBoundarySize - layers * 10))) - ((int)softBodyBoundarySize - layers * 10), -softBodyBoundarySize + layers * 10);
				break;
			}
		}
		theAI = new SoftBodyAI(m_physicsScene, layers, 4, 200000, position, 1.01f, 1.0f, direction, &softBodiesList);
		softBodiesList.push_back(theAI);
	}
}

void PhysicsEngineApp::softBodiesUpdate(float deltaTime)
{

	if (thePlayer->isAlive())
	{
		aie::Input* input = aie::Input::getInstance();

		if (input->isKeyDown(aie::INPUT_KEY_LEFT))
		{
			thePlayer->left(true);
		}
		if (input->isKeyDown(aie::INPUT_KEY_RIGHT))
		{
			thePlayer->right(true);
		}
		if (input->isKeyDown(aie::INPUT_KEY_UP))
		{
			thePlayer->up(true);
		}
		if (input->isKeyDown(aie::INPUT_KEY_DOWN))
		{
			thePlayer->down(true);
		}
		if (input->isKeyDown(aie::INPUT_KEY_SPACE))
		{
			thePlayer->all(true);
		}
		if (input->wasKeyReleased(aie::INPUT_KEY_LEFT))
		{
			thePlayer->left(false);
		}
		if (input->wasKeyReleased(aie::INPUT_KEY_RIGHT))
		{
			thePlayer->right(false);
		}
		if (input->wasKeyReleased(aie::INPUT_KEY_UP))
		{
			thePlayer->up(false);
		}
		if (input->wasKeyReleased(aie::INPUT_KEY_DOWN))
		{
			thePlayer->down(false);
		}
		if (input->wasKeyReleased(aie::INPUT_KEY_SPACE))
		{
			thePlayer->all(false);
		}

		if (thePlayer->getPosition().y > 960)
		{
			thePlayer->goBoom(m_physicsScene);
			winner = true;
		}
	}

	for (int i = 0; i < softBodiesList.size(); i++)
	{
		softBodiesList[i]->update(deltaTime, softBodyBoundarySize);
	}

	if (softBodiesList.size() < totalSoftBodies)
	{
		int layers = rand() % 6 + 3;
		Direction direction = Direction(rand() % 4);
		glm::vec2 position;
		switch (direction)
		{
		case 0:
		{
			position = glm::vec2(-softBodyBoundarySize + layers * 6, (rand() % (2 * ((int)softBodyBoundarySize - layers * 6))) - ((int)softBodyBoundarySize - layers * 6));
			break;
		}
		case 1:
		{
			position = glm::vec2(softBodyBoundarySize - layers * 6, (rand() % (2 * ((int)softBodyBoundarySize - layers * 6))) - ((int)softBodyBoundarySize - layers * 6));
			break;
		}
		case 2:
		{
			position = glm::vec2((rand() % (2 * ((int)softBodyBoundarySize - layers * 6))) - ((int)softBodyBoundarySize - layers * 6), softBodyBoundarySize - layers * 6);
			break;
		}
		case 3:
		{
			position = glm::vec2((rand() % (2 * ((int)softBodyBoundarySize - layers * 6))) - ((int)softBodyBoundarySize - layers * 6), -softBodyBoundarySize + layers * 6);
			break;
		}
		}
		theAI = new SoftBodyAI(m_physicsScene, layers, 4, 200000, position, 1.01f, 1.0f, direction, &softBodiesList);
		softBodiesList.push_back(theAI);
	}
}

float PhysicsEngineApp::convertXGizmoToBoot(float posInGizmo)
{
	
	return (posInGizmo * (6.4f) + getWindowWidth());
}

float PhysicsEngineApp::convertYGizmoToBoot(float posInGizmo)
{
	return (posInGizmo * (6.4f) + getWindowHeight());
}
