#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include <time.h>
#include <imgui.h>

#include "Gizmos.h"

#include "CosmeticSphere.h"
#include "CosmeticBox.h"
#include "Sphere.h"
#include "Box.h"
#include "Spring.h"
#include "String.h"
#include "PhysicsScene.h"
#include "ShapeBuilder.h"
#include "SoftBodyPlayer.h"
#include "SoftBodyAI.h"

typedef void(*fncP)();

class PhysicsEngineApp : public aie::Application {
public:

	PhysicsEngineApp();
	virtual ~PhysicsEngineApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

	void startUpMenu();
	void basicShapeTesting();
	void springTesting();
	void art();
	void letterFall();
	void letterFallUpdate(float deltaTime);
	void softBodies();
	void softBodiesUpdate(float deltaTime);
	float convertXGizmoToBoot(float posInGizmo);
	float convertYGizmoToBoot(float posInGizmo);



protected:

	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;

	char* buffer;
	bool letterFallActive = false;
	bool softBodiesActive = false;
	bool displayScenes = false;
	PhysicsScene* m_physicsScene;
	float rocketMass;
	int sceneSelector;
	float fuelUsed;
	int counter = 10;
	float totalScenes = 5.0f;
	Sphere* rocket;
	ShapeBuilder shapeBuilder;
	SoftBodyPlayer* thePlayer;
	int totalSoftBodies = 6;
	float softBodyBoundarySize = 200;
	std::vector<Box*> underwaterRocksList;
	std::vector<SoftBodyAI*> softBodiesList;
	SoftBodyAI* theAI;
	aie::Texture* backgroundBottom, *backgroundMiddle, *backgroundTop, *rock, *victory;
	bool winner = false;

};
