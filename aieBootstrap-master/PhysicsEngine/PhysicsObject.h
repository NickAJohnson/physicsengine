#pragma once
#include <glm/glm.hpp>

enum ShapeType {
	COSMETIC_BOX = -3,
	COSMETIC_SPHERE,
	JOINT,
	PLANE,
	SPHERE,
	BOX,
	SHAPE_COUNT,
	AI_SPHERE
};

class PhysicsObject
{
protected:
	PhysicsObject(ShapeType a_shapeID) : m_shapeID(a_shapeID){}

public:
	virtual void fixedUpdate(glm::vec2 gravity, float timeStep) = 0;
	virtual void debug() = 0;
	virtual void makeGizmo() = 0;
	virtual void resetPosition() {};
	void setShapeID(ShapeType newShapeType) { m_shapeID = newShapeType; }

	int getShapeID() const { return m_shapeID; }


protected:
	ShapeType m_shapeID;

};

