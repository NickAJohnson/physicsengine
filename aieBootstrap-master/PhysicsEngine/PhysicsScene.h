#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <algorithm>
#include <iostream>
#include <list>
#include <cmath>
#include <string>
#include <iostream>
#include "Application.h"
#include "Font.h"
#include "Renderer2D.h"
#include "PhysicsObject.h"
#include "RigidBody.h"
#include "Sphere.h"
#include "Plane.h"
#include "Box.h"

class PhysicsScene
{
public:
	PhysicsScene(int windowWidth, int windowHeight);
	~PhysicsScene();

	void addActor(PhysicsObject* actor);
	void clearScene();
	void removeActor(PhysicsObject* actor);
	void update(float dt);
	void updateGizmos();
	void debugScene();

	void setGravity(const glm::vec2 gravity) { m_gravity = gravity; }
	glm::vec2 getGravity() const { return m_gravity; }

	void setTimeStep(const float timeStep) { m_timeStep = timeStep; }
	float getTimeStep() const { return m_timeStep; }

	void displayEnergy(aie::Font* m_font, aie::Renderer2D* m_2dRenderer);

	int convertXGizmoToBoot(float posInGizmo);
	int convertYGizmoToBoot(float posInGizmo);

	void checkForCollision();

	static void ApplyContactForces(RigidBody* body1, RigidBody* body2, glm::vec2 norm, float pen);

	static bool plane2Plane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool plane2Sphere(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool plane2Box(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool sphere2Plane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool sphere2Sphere(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool sphere2Box(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool box2Plane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool box2Sphere(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool box2Box(PhysicsObject* obj1, PhysicsObject* obj2);

protected:
	glm::vec2 m_gravity;
	float m_timeStep;
	std::vector<PhysicsObject*> m_actors;
	int m_windowWidth;
	int m_windowHeight;
};

