#include "RigidBody.h"



RigidBody::RigidBody(ShapeType shapeID, glm::vec2 position, glm::vec2 velocity, float rotation, float mass) : PhysicsObject(shapeID)
{
	m_position = position;
	m_velocity = velocity;
	m_mass = mass;
	m_rotation = rotation;
	m_angularVelocity = 0;
	m_angularDrag = 0.3f;
	m_linearDrag = 0.0f;
	m_elasticity = 0.7f;
	m_isKinematic = false;
	m_kineticEnergy = 0.0f;
}

RigidBody::~RigidBody()
{
}

void RigidBody::fixedUpdate(glm::vec2 gravity, float timeStep)
{
	if (m_isKinematic)
	{
		m_velocity = glm::vec2(0, 0);
		m_angularVelocity = 0;
		return;
	}
	m_position += m_velocity * timeStep;
	m_velocity += gravity * timeStep;

	m_velocity -= m_linearDrag * m_velocity * timeStep;
	m_rotation += m_angularVelocity * timeStep;
	m_angularVelocity -= m_angularDrag * m_angularVelocity * timeStep;

	if (length(m_velocity) < MIN_LINEAR_THRESHOLD)
	{
			m_velocity = glm::vec2(0, 0);
	}
	
	if (abs(m_angularVelocity) < MIN_ROTATION_THRESHOLD)
	{
		m_angularVelocity = 0;
	}

	//Calc kinetic energy
	m_kineticEnergy = (1.0f / 2.0f)*m_mass*(glm::length(m_velocity)*glm::length(m_velocity)) + ((1/2) * m_moment * (m_angularVelocity * m_angularVelocity));
}

void RigidBody::applyForce(glm::vec2 force, glm::vec2 pos)
{
	m_velocity += force / m_mass;
	m_angularVelocity += (force.y * pos.x - force.x * pos.y) / (m_moment);
}

void RigidBody::setVelocity(glm::vec2 newVelocity)
{
	m_velocity = newVelocity;
}

void RigidBody::setAngularDrag(float newAngularDrag)
{
	m_angularDrag = newAngularDrag;
}

void RigidBody::setElasticity(float newElasticity)
{
	m_elasticity = newElasticity;
}

void RigidBody::setPosition(glm::vec2 newPosition)
{
	m_position = newPosition;
}

void RigidBody::setKinematic(bool state)
{
	m_isKinematic = state;
	m_mass = INT_MAX;
	m_moment = INT_MAX;
}

glm::vec2 RigidBody::toWorld(glm::vec2 pos) const
{
	if (getShapeID() == BOX || getShapeID() == COSMETIC_BOX)
	{
		return (m_position + pos.x * m_localX + pos.y * m_localY);
	}
	else
	{
		return m_position;
	}
}

void RigidBody::setLinearDrag(float newLinearDrag)
{
	m_linearDrag = newLinearDrag;
}

void RigidBody::resolveCollision(RigidBody * actor2, glm::vec2 contact, glm::vec2* collisionNormal)
{
	//find the vector between their centres, or use the provided direction of force and make sure it's normalised
	glm::vec2 normal = glm::normalize(collisionNormal ? *collisionNormal : actor2->m_position - m_position);
	//get the vector perpendicular to the collision normal
	glm::vec2 perp(normal.y, -normal.x);

	//determine the total velocity of the contact points for the two objects for both linear and rotational

		//'r' is the radius from the axis to the application of force
	float r1 = glm::dot(contact - m_position, -perp);
	float r2 = glm::dot(contact - actor2->m_position, perp);
		//velocity of the contact point on this object
	float v1 = glm::dot(m_velocity, normal) - r1 * m_angularVelocity;
		//velocity of contact point on actor2
	float v2 = glm::dot(actor2->m_velocity, normal) + r2 * actor2->m_angularVelocity;
	if (v1 > v2) //they're moving closer
	{
		//calculate the effective mass at contact point for each object
		//ie how much the contact point will move due to the force applied.
		float mass1 = 1.0f / (1.0f / getMass() + (r1*r1) / getMoment());
		float mass2 = 1.0f / (1.0f / actor2->getMass() + (r2*r2) / actor2->getMoment());

		float elasticity = (m_elasticity + actor2->getElasticity()) / 2.0f;
		glm::vec2 force = (1.0f + elasticity)*mass1*mass2 / (mass1 + mass2)*(v1 - v2)*normal;
		
		//apply equal and opposite forces
		applyForce(-force, contact - m_position);
		actor2->applyForce(force, contact - actor2->m_position);
	}
}
