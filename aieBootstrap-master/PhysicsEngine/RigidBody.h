#pragma once
#include "PhysicsObject.h"

const float MIN_LINEAR_THRESHOLD = 0.1f;
const float MIN_ROTATION_THRESHOLD = 0.01f;

class RigidBody :
	public PhysicsObject
{
public:
	RigidBody(ShapeType shapeID, glm::vec2 position, glm::vec2 velocity, float rotation, float mass);
	~RigidBody();

	virtual void fixedUpdate(glm::vec2 gravity, float timeStep);
	virtual void debug() {};
	//check
	void applyForce(glm::vec2 force, glm::vec2 pos);
	//void applyForceToActor(RigidBody* actor2, glm::vec2 force);


	void setVelocity(glm::vec2 newVelocity);
	void setLinearDrag(float newLinearDrag);
	void setAngularDrag(float newAngularDrag);
	void setElasticity(float newElasticity);
	void setPosition(glm::vec2 newPosition);
	void setKinematic(bool state);

	glm::vec2 getPosition() const { return m_position; }
	glm::vec2 toWorld(glm::vec2 pos) const;
	float getRotation() const { return m_rotation; }
	glm::vec2 getVelocity() const { return m_velocity; }
	float getAngularVelocity() const { return m_angularVelocity; }
	float getMass() const { return (m_isKinematic) ? INT_MAX : m_mass; }
	float getMoment() const { return (m_isKinematic) ? INT_MAX : m_moment; }
	float getLinearDrag() const { return m_linearDrag; }
	float getAngularDrag() const { return m_angularDrag; }
	float getElasticity() const { return m_elasticity; }
	bool isKinematic() const { return m_isKinematic; }
	float getKineticEnergy() const { return m_kineticEnergy; }

	void resolveCollision(RigidBody* actor2, glm::vec2 contact, glm::vec2* collisionNormal=nullptr);

protected:
	glm::vec2 m_position;
	glm::vec2 m_velocity;
	float m_mass;
	float m_rotation;  //2D so we only need a single float to represent our rotation
	float m_linearDrag;
	float m_angularDrag;
	float m_elasticity;
	float m_angularVelocity;
	float m_moment;
	bool m_isKinematic;
	float m_kineticEnergy;

	glm::vec2 m_localX;
	glm::vec2 m_localY;
};

