#include "ShapeBuilder.h"



ShapeBuilder::ShapeBuilder()
{
}


ShapeBuilder::~ShapeBuilder()
{
}

void ShapeBuilder::update(PhysicsScene * scene, float deltaTime)
{
	if (letterDelay <= 0)
	{
		if (letterQueue.length() > 0)
		{
			letterToShape(scene, letterQueue[0]);
			letterQueue.erase(0, 1);
			letterDelay = .6f;
		}
		else
		{
			return;
		}
	}
	else
	{
		letterDelay -= deltaTime;
	}
}

void ShapeBuilder::addToString(std::string text)
{
	letterQueue += text;
}

void ShapeBuilder::letterToShape(PhysicsScene * scene, char letter)
{
	std::vector<std::string> asciiArt;


	switch((int)letter)
	{
	case 65: case 97: // a/A
		{
			asciiArt.push_back("..0..");
			asciiArt.push_back(".0.0."); 
			asciiArt.push_back(".000.");
			asciiArt.push_back(".0.0.");
			asciiArt.push_back(".0.0.");
			createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
			//return when shape made - then do timer till another letter
			break;
		}
	case 66: case 98: // b/B
	{
		asciiArt.push_back("0000.");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0000.");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0000.");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 67: case 99: // c/C
	{
		asciiArt.push_back("00000");
		asciiArt.push_back("0....");
		asciiArt.push_back("0....");
		asciiArt.push_back("0....");
		asciiArt.push_back("00000");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 68: case 100: // d/D
	{
		asciiArt.push_back("0000.");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0000.");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 69: case 101: // e/E
	{
		asciiArt.push_back("00000");
		asciiArt.push_back("0....");
		asciiArt.push_back("0000.");
		asciiArt.push_back("0....");
		asciiArt.push_back("00000");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 70: case 102: // f/F
	{
		asciiArt.push_back("00000");
		asciiArt.push_back("0....");
		asciiArt.push_back("000..");
		asciiArt.push_back("0....");
		asciiArt.push_back("0....");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 71: case 103: // g/G
	{
		asciiArt.push_back("00000");
		asciiArt.push_back("0....");
		asciiArt.push_back("0.000");
		asciiArt.push_back("0...0");
		asciiArt.push_back("00000");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 72: case 104: // h/H
	{
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		asciiArt.push_back("00000");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 73: case 105: // i/I
	{
		asciiArt.push_back("00000");
		asciiArt.push_back("..0..");
		asciiArt.push_back("..0..");
		asciiArt.push_back("..0..");
		asciiArt.push_back("00000");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 74: case 106: // j/J
	{
		asciiArt.push_back("00000");
		asciiArt.push_back("..0..");
		asciiArt.push_back("..0..");
		asciiArt.push_back("0.0..");
		asciiArt.push_back("000..");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 75: case 107: // k/K
	{
		asciiArt.push_back("0..0.");
		asciiArt.push_back("0.0..");
		asciiArt.push_back("00...");
		asciiArt.push_back("0.0..");
		asciiArt.push_back("0..0.");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 76: case 108: // l/L
	{
		asciiArt.push_back("0....");
		asciiArt.push_back("0....");
		asciiArt.push_back("0....");
		asciiArt.push_back("0....");
		asciiArt.push_back("00000");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 77: case 109: // m/M
	{
		asciiArt.push_back("00000");
		asciiArt.push_back("0.0.0");
		asciiArt.push_back("0.0.0");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 78: case 110: // n/N
	{
		asciiArt.push_back("0...0");
		asciiArt.push_back("00..0");
		asciiArt.push_back("0.0.0");
		asciiArt.push_back("0..00");
		asciiArt.push_back("0...0");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 79: case 111: // o/O
	{
		asciiArt.push_back(".000.");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		asciiArt.push_back(".000.");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 80: case 112: // p/P
	{
		asciiArt.push_back("0000.");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0000.");
		asciiArt.push_back("0....");
		asciiArt.push_back("0....");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 81: case 113: // q/Q
	{
		asciiArt.push_back(".00..");
		asciiArt.push_back("0..0.");
		asciiArt.push_back("0..0.");
		asciiArt.push_back("0..0.");
		asciiArt.push_back(".00.0");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 82: case 114: // r/R
	{
		asciiArt.push_back("0000.");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0000.");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 83: case 115: // s/S
	{
		asciiArt.push_back("00000");
		asciiArt.push_back("0....");
		asciiArt.push_back("00000");
		asciiArt.push_back("....0");
		asciiArt.push_back("00000");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 84: case 116: // t/T
	{
		asciiArt.push_back("00000");
		asciiArt.push_back("..0..");
		asciiArt.push_back("..0..");
		asciiArt.push_back("..0..");
		asciiArt.push_back("..0..");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 85: case 117: // u/U
	{
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		asciiArt.push_back("00000");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 86: case 118: // v/V
	{
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		asciiArt.push_back(".0.0.");
		asciiArt.push_back("..0..");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 87: case 119: // w/W
	{
		asciiArt.push_back("0...0");
		asciiArt.push_back("0...0");
		asciiArt.push_back("0.0.0");
		asciiArt.push_back("0.0.0");
		asciiArt.push_back("00000");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 88: case 120: // x/X
	{
		asciiArt.push_back("0...0");
		asciiArt.push_back(".0.0.");
		asciiArt.push_back("..0..");
		asciiArt.push_back(".0.0.");
		asciiArt.push_back("0...0");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 89: case 121: // y/Y
	{
		asciiArt.push_back("0...0");
		asciiArt.push_back(".0.0.");
		asciiArt.push_back("..0..");
		asciiArt.push_back("..0..");
		asciiArt.push_back("..0..");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}
	case 90: case 122: // z/Z
	{
		asciiArt.push_back("00000");
		asciiArt.push_back("...0.");
		asciiArt.push_back("..0..");
		asciiArt.push_back(".0...");
		asciiArt.push_back("00000");
		createShape(scene, asciiArt, glm::vec2(0, 0), 5.0f);
		//return when shape made - then do timer till another letter
		break;
	}

		default:
		{
			break;
		}
	}


}

void ShapeBuilder::createShape(PhysicsScene * scene, std::vector<std::string>& text, glm::vec2 position, float distanceApart)
{
	Sphere* sphere;
	int amountOfStrings = text.size(); 
	int longestString = text[0].length();
	//Find longest string
	for (int i = 1; i < amountOfStrings; i++)
	{
		if (text[i].length() > longestString)
		{
			int longestString = text[i].length();
		}
	}
	//fill strings to same length
	for (int i = 0; i < amountOfStrings; i++)
	{
		while (text[i].length() < longestString)
		{
			text[i].append(".");
		}
	}

	//CreateSpheres
	for (int i = 0; i < amountOfStrings; i++)
	{
		distanceApart = 10.0f;
		for (int j = 0; j < longestString; j++)
		{
			if (text[i].at(j) == '0')
			{
				sphere = new Sphere(glm::vec2((-longestString * distanceApart / 2.0f ) + j*distanceApart, 80 - i*distanceApart), glm::vec2(0, -100), 5.0f, 3.0f, glm::vec4(0, 0, 1, 1));
				scene->addActor(sphere);
			}
			else
			{
				std::cout << text[i].at(j) << std::endl;
			}
		}
	}
}
