#pragma once
#include <string>
#include <vector>
#include "Sphere.h"
#include "PhysicsScene.h"


class ShapeBuilder
{
public:
	ShapeBuilder();
	~ShapeBuilder();

	void update(PhysicsScene * scene, float deltaTime);
	void addToString(std::string text);
	void letterToShape(PhysicsScene* scene, char letter);
	void createShape(PhysicsScene* scene, std::vector<std::string>& text, glm::vec2 position, float distanceApart);

protected:
	std::string letterQueue;
	float letterDelay = 0;
	int m_windowHeight = 800;
	int m_windowWidth = 1280;
};

