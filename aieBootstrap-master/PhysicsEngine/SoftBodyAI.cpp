#include "SoftBodyAI.h"

SoftBodyAI::~SoftBodyAI()
{
}

void SoftBodyAI::update(float deltaTime, float boundarySize)
{
	if (m_timer > 0)
	{
		m_timer -= deltaTime;
		if (m_timer <= 0)
		{
			switch (m_direction)
			{
				case 0:
				{
					left(true);
					m_releaseTimer = m_releaseTimerReset;
					m_releaseDirection = Direction::LEFT;
					break;
				}
				case 1:
				{
					right(true);
					m_releaseTimer = m_releaseTimerReset;
					m_releaseDirection = Direction::RIGHT;
					break;
				}
				case 2:
				{
					up(true);
					m_releaseTimer = m_releaseTimerReset;
					m_releaseDirection = Direction::UP;
					break;
				}
				case 3:
				{
					down(true);
					m_releaseTimer = m_releaseTimerReset;
					m_releaseDirection = Direction::DOWN;
					break;
				}
				default:
				{
					break;
				}
			}
		}
	}
	else
	{
		m_releaseTimer -= deltaTime;
		if (m_releaseTimer < 0)
		{
			switch (m_releaseDirection)
			{
				case 0:
				{
					left(false);
					m_timer = m_timerReset;
					break;
				}
				case 1:
				{
					right(false);
					m_timer = m_timerReset;
					break;
				}
				case 2:
				{
					up(false);
					m_timer = m_timerReset;
					break;
				}
				case 3:
				{
					down(false);
					m_timer = m_timerReset;
					break;
				}
				default:
				{
					break;
				}
			}
		}
	}
	
	glm::vec2 position = getPosition();
	if (position.x > boundarySize - 30 && m_direction != RIGHT)
	{
		m_direction = RIGHT;
	}
	else if (position.x < -boundarySize + 30 && m_direction != LEFT)
	{
		m_direction = LEFT;
	}
	else if (position.y > boundarySize - 30 && m_direction != UP)
	{
		m_direction = UP;
	}
	else if (position.y < -boundarySize + 30 && m_direction != DOWN)
	{
		m_direction = DOWN;
	}
}

void SoftBodyAI::goBoom(PhysicsScene* scene)
{
	for (int i = 0; i < springList.size(); i++)
	{
		if (springList[i] != nullptr)
		{
			if(rand()%2 == 1)
			scene->removeActor(springList[i]);
		}
	}
	std::vector<SoftBodyAI*>::iterator it = std::find(m_aiList->begin(), m_aiList->end(), this);
	int index = std::distance(m_aiList->begin(), it);
     m_aiList->erase(m_aiList->begin()+index);
	 for (int i = 0; i < sphereList.size(); i++)
	 {
		 if (sphereList[i] != nullptr)
		 {
			 sphereList[i]->setShapeID(COSMETIC_SPHERE);
		 }
	 }
}