#pragma once
#include <vector>
#include "SoftBodyPlayer.h"

enum Direction {

	LEFT = 0,
	RIGHT,
	UP,
	DOWN
};

class SoftBodyAI :
	public SoftBodyPlayer
{
public:
	SoftBodyAI(PhysicsScene* scene, int layers, float distanceApart, float strength, glm::vec2 position, 
		float expansion, float ballSize, Direction direction, std::vector<SoftBodyAI*>* aiList)
		: SoftBodyPlayer(scene, layers, distanceApart, strength, position, expansion, ballSize, true, this) 
	{
		m_direction = direction;
		m_aiList = aiList;
	}
	~SoftBodyAI();

	void update(float deltaTime, float boundarySize);
	void goBoom(PhysicsScene* scene);
	


private:
	float m_timer = 1.0f;
	float m_timerReset = 1.0f;
	float m_releaseTimer;
	float m_releaseTimerReset = 0.5f;

	Direction m_releaseDirection;
	Direction m_direction;
	std::vector<SoftBodyAI*>* m_aiList;
};

