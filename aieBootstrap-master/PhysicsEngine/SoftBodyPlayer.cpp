#include "SoftBodyPlayer.h"
#include "Sphere.h"

SoftBodyPlayer::SoftBodyPlayer(PhysicsScene* scene, int layers, float distanceApart, float strength, glm::vec2 position, float expansion, float ballSize, bool isAI, SoftBodyAI* parent)
{
	m_expansion = expansion;
	m_distanceApart = distanceApart;
	int strLength = layers * 2 - 1;
	std::vector<std::string> ascii;
	//Loop for each string layer (row)
	for (int i = 0; i < layers; i++)
	{
		std::string tempString = "";
		int j = layers - 1 - i;
		while (j > 0)
		{
			tempString += ".";
			j--;
		}
		//Reset k, add a 0 for each layer from centre, 2 if not first loop
		j = layers - 1 - i;
		bool first0 = true;
		while (j < layers)
		{
			tempString += "0";
			if (!first0)
			{
				tempString += "0";
			}
			first0 = false;
			j++;
		}
		//Add remaining dots
		j = layers - 1 - i;
		while (j > 0)
		{
			tempString += ".";
			j--;
		}
		
		//Add string to ascii vector
		ascii.push_back(tempString);
	}

	//Duplicate strings (excluding middle)
	for (int i = 0; i < layers - 1; i++)
	{
		std::string tempString = ascii[layers - 2 - i];
		ascii.push_back(tempString);
	}
	createShape(scene, ascii, position, distanceApart, strength, ballSize);
	if (isAI)
	{
		for (int i = 0; i < sphereList.size(); i++)
		{
			if (sphereList[i] != nullptr)
			{
				sphereList[i]->setShapeID(ShapeType::AI_SPHERE);
				sphereList[i]->setParent(parent, scene);
			}
		}
	}
}

SoftBodyPlayer::~SoftBodyPlayer()
{
}

void SoftBodyPlayer::createShape(PhysicsScene * scene, std::vector<std::string>& text, glm::vec2 position, float distanceApart, float strength, float ballSize)
{
	Sphere* sphere00, *sphere01, *sphere11, *sphere10;
	int amountOfStrings = text.size();
	int longestString = text[0].length();

	//CreateSpheres
	for (int i = 0; i < amountOfStrings; i++)
	{
		for (int j = 0; j < longestString; j++)
		{
			if (text[i].at(j) == '0')
			{
				sphere00 = new Sphere(glm::vec2((position.x + (-longestString * distanceApart / 2.0f) + j * distanceApart), position.y - i * distanceApart), glm::vec2(0, 0), 1.0f, ballSize, glm::vec4(0, 0, 1, 1));
				scene->addActor(sphere00);
			}
			else
			{
				sphere00 = nullptr;
			}
			sphereList.push_back(sphere00);
		}
	}

	for (int i = 0; i < amountOfStrings-1; i++)
	{
		for (int j = 0; j < longestString-1; j++)
		{
			//Sphere00
			if (j == 0)
			{
				sphere00 = sphereList[i*longestString];
			}
			else
			{
				sphere00 = sphere01;
			}

			//sphere10
			if (j == 0)
			{
				sphere10 = sphereList[(i+1)*longestString];
			}
			else
			{
				sphere10 = sphere11;
			}

			//sphere01
			sphere01 = sphereList[i * longestString + j + 1];
			//sphere11
			sphere11 = sphereList[(i+1) * longestString + j + 1];
			strength = 10;

			//Create Springs
			if (i == 0 && j == 0)
			{
				squareConnect(scene, sphere00, sphere10, sphere01, sphere11, distanceApart, strength);
			}
			else if (i == 0 && j != 0)
			{
				noLeftConnect(scene, sphere00, sphere10, sphere01, sphere11, distanceApart, strength);
			}
			else if (i != 0 && j == 0)
			{
				noRoofConnect(scene, sphere00, sphere10, sphere01, sphere11, distanceApart, strength);
			}
			else
			{
				cornerConnect(scene, sphere00, sphere10, sphere01, sphere11, distanceApart, strength);
			}
		}
	}
}

void SoftBodyPlayer::squareConnect(PhysicsScene * scene, Sphere * sphere00, Sphere * sphere10, Sphere * sphere01, Sphere * sphere11, float distanceApart, float strength)
{
	
	Spring* spring;
	//Sides
	if (sphere00 != nullptr && sphere01 != nullptr)
	{
		spring = new Spring(sphere00, sphere01, distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	if (sphere10 != nullptr && sphere11 != nullptr)
	{
		spring = new Spring(sphere10, sphere11, distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	if (sphere00 != nullptr && sphere10 != nullptr)
	{
		spring = new Spring(sphere00, sphere10, distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	if (sphere01 != nullptr && sphere11 != nullptr)
	{
		spring = new Spring(sphere01, sphere11, distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	//Corners
	if (sphere00 != nullptr && sphere11 != nullptr)
	{
		spring = new Spring(sphere00, sphere11, 1.41421f * distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	if (sphere10 != nullptr && sphere01 != nullptr)
	{
		spring = new Spring(sphere10, sphere01, 1.41421f * distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}

}

void SoftBodyPlayer::noRoofConnect(PhysicsScene * scene, Sphere * sphere00, Sphere * sphere10, Sphere * sphere01, Sphere * sphere11, float distanceApart, float strength)
{
	Spring* spring;
	//Sides
	if (sphere10 != nullptr && sphere11 != nullptr)
	{
		spring = new Spring(sphere10, sphere11, distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	if (sphere00 != nullptr && sphere10 != nullptr)
	{
		spring = new Spring(sphere00, sphere10, distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	if (sphere01 != nullptr && sphere11 != nullptr)
	{
		spring = new Spring(sphere01, sphere11, distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	//Corners
	if (sphere00 != nullptr && sphere11 != nullptr)
	{
		spring = new Spring(sphere00, sphere11, 1.41421f * distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	if (sphere10 != nullptr && sphere01 != nullptr)
	{
		spring = new Spring(sphere10, sphere01, 1.41421f * distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
}

void SoftBodyPlayer::noLeftConnect(PhysicsScene * scene, Sphere * sphere00, Sphere * sphere10, Sphere * sphere01, Sphere * sphere11, float distanceApart, float strength)
{
	Spring* spring;
	//Sides
	if (sphere00 != nullptr && sphere01 != nullptr)
	{
		spring = new Spring(sphere00, sphere01, distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	if (sphere10 != nullptr && sphere11 != nullptr)
	{
		spring = new Spring(sphere10, sphere11, distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	if (sphere01 != nullptr && sphere11 != nullptr)
	{
		spring = new Spring(sphere01, sphere11, distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	//Corners
	if (sphere00 != nullptr && sphere11 != nullptr)
	{
		spring = new Spring(sphere00, sphere11, 1.41421f * distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	if (sphere10 != nullptr && sphere01 != nullptr)
	{
		spring = new Spring(sphere10, sphere01, 1.41421f * distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
}

void SoftBodyPlayer::cornerConnect(PhysicsScene * scene, Sphere * sphere00, Sphere * sphere10, Sphere * sphere01, Sphere * sphere11, float distanceApart, float strength)
{
	Spring* spring;
	//Sides
	if (sphere10 != nullptr && sphere11 != nullptr)
	{
		spring = new Spring(sphere10, sphere11, distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	if (sphere01 != nullptr && sphere11 != nullptr)
	{
		spring = new Spring(sphere01, sphere11, distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	//Corners
	if (sphere00 != nullptr && sphere11 != nullptr)
	{
		spring = new Spring(sphere00, sphere11, 1.41421f * distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
	if (sphere10 != nullptr && sphere01 != nullptr)
	{
		spring = new Spring(sphere10, sphere01, 1.41421f * distanceApart, strength, globalDampness);
		scene->addActor(spring);
		springList.push_back(spring);
	}
}

glm::vec2 SoftBodyPlayer::getPosition()
{
	//Finding approximate centre of spheres
	float totalX = 0;
	float totalY = 0;
	int sphereCounter = 0;
	for (int i = 0; i < sphereList.size(); i++)
	{
		if (sphereList[i] != nullptr)
		{
			totalX += sphereList[i]->getPosition().x;
			totalY += sphereList[i]->getPosition().y;
			sphereCounter++;
		}
	}
	return glm::vec2(totalX / sphereCounter, totalY / sphereCounter);
}

float SoftBodyPlayer::getCamX()
{
	return (getPosition().x * (6.4f) + (m_windowWidth / 2)) - 3;
}

float SoftBodyPlayer::getCamY()
{
	return (getPosition().y * (6.4f) + (m_windowHeight / 2)) - 3;
}

void SoftBodyPlayer::right(bool status)
{
	m_center = getPosition();

	glm::vec2 springPosition, spherePosition;

	//Right Expansion
	if (status)
	{
		for (int i = 0; i < springList.size(); i++)
		{
			springPosition = springList[i]->getCenter() - m_center;
			if (springPosition.x > 0 && springPosition.x > abs(springPosition.y))
			{
				springList[i]->expand(true);
			}
		}
		for (int i = 0; i < sphereList.size(); i++)
		{
			if (sphereList[i] != nullptr)
			{
				spherePosition = sphereList[i]->getPosition() - m_center;
				if (spherePosition.x > 0 && spherePosition.x > abs(spherePosition.y))
				{
					sphereList[i]->weighted(true);
				}
			}
		}
	}
	else
	{
		for (int i = 0; i < springList.size(); i++)
		{
			springList[i]->expand(false);
		}

		for (int i = 0; i < sphereList.size(); i++)
		{
			if (sphereList[i] != nullptr)
			{
				sphereList[i]->weighted(false);
			}
		}
	}
}

void SoftBodyPlayer::left(bool status)
{
	//Finding approximate centre of spheres
	float totalX = 0;
	float totalY = 0;
	int sphereCounter = 0;
	for (int i = 0; i < sphereList.size(); i++)
	{
		if (sphereList[i] != nullptr)
		{
			totalX += sphereList[i]->getPosition().x;
			totalY += sphereList[i]->getPosition().y;
			sphereCounter++;
		}
	}
	m_center = glm::vec2(totalX / sphereCounter, totalY / sphereCounter);

	glm::vec2 springPosition, spherePosition;

	//Left Expansion
	if (status)
	{
		for (int i = 0; i < springList.size(); i++)
		{
			springPosition = springList[i]->getCenter() - m_center;

			if (springPosition.x < 0 && abs(springPosition.x) > abs(springPosition.y))
			{
				springList[i]->expand(true);
			}
		}
		for (int i = 0; i < sphereList.size(); i++)
		{
			if (sphereList[i] != nullptr)
			{
				spherePosition = sphereList[i]->getPosition() - m_center;
				if (spherePosition.x < 0 && abs(spherePosition.x) > abs(spherePosition.y))
				{
					sphereList[i]->weighted(true);
				}
			}
		}
	}
	else
	{
		for (int i = 0; i < springList.size(); i++)
		{
			springList[i]->expand(false);
		}
		for (int i = 0; i < sphereList.size(); i++)
		{
			if (sphereList[i] != nullptr)
			{
				sphereList[i]->weighted(false);
			}
		}
	}
}

void SoftBodyPlayer::down(bool status)
{
	//Finding approximate centre of spheres
	float totalX = 0;
	float totalY = 0;
	int sphereCounter = 0;
	for (int i = 0; i < sphereList.size(); i++)
	{
		if (sphereList[i] != nullptr)
		{
			totalX += sphereList[i]->getPosition().x;
			totalY += sphereList[i]->getPosition().y;
			sphereCounter++;
		}
	}
	m_center = glm::vec2(totalX / sphereCounter, totalY / sphereCounter);

	glm::vec2 springPosition, spherePosition;
	//Up Expansion
	if (status)
	{
		for (int i = 0; i < springList.size(); i++)
		{
			springPosition = springList[i]->getCenter() - m_center;
			if (springPosition.y < 0 && abs(springPosition.y) > abs(springPosition.x))
			{
				springList[i]->expand(true);
			}
		}
		for (int i = 0; i < sphereList.size(); i++)
		{
			if (sphereList[i] != nullptr)
			{
				spherePosition = sphereList[i]->getPosition() - m_center;
				if (spherePosition.y < 0 && spherePosition.y > abs(spherePosition.x))
				{
					sphereList[i]->weighted(true);
				}
			}
		}
	}
	else
	{
		for (int i = 0; i < springList.size(); i++)
		{
			springList[i]->expand(false);
		}
		for (int i = 0; i < sphereList.size(); i++)
		{
			if (sphereList[i] != nullptr)
			{
				sphereList[i]->weighted(false);
			}
		}
	}
}

void SoftBodyPlayer::up(bool status)
{
	//Finding approximate centre of spheres
	float totalX = 0;
	float totalY = 0;
	int sphereCounter = 0;
	for (int i = 0; i < sphereList.size(); i++)
	{
		if (sphereList[i] != nullptr)
		{
			totalX += sphereList[i]->getPosition().x;
			totalY += sphereList[i]->getPosition().y;
			sphereCounter++;
		}
	}
	m_center = glm::vec2(totalX / sphereCounter, totalY / sphereCounter);

	glm::vec2 springPosition, spherePosition;
	//Up Expansion
	if (status)
	{
		for (int i = 0; i < springList.size(); i++)
		{
			springPosition = springList[i]->getCenter() - m_center;
			if (springPosition.y > 0 && springPosition.y > abs(springPosition.x))
			{
				springList[i]->expand(true);
			}
		}
		for (int i = 0; i < sphereList.size(); i++)
		{
			if (sphereList[i] != nullptr)
			{
				spherePosition = sphereList[i]->getPosition() - m_center;
				if (spherePosition.y > 0 && spherePosition.y > abs(spherePosition.x))
				{
					sphereList[i]->weighted(true);
				}
			}
		}
	}
	else
	{
		for (int i = 0; i < springList.size(); i++)
		{
			springList[i]->expand(false);
		}
		for (int i = 0; i < sphereList.size(); i++)
		{
			if (sphereList[i] != nullptr)
			{
				sphereList[i]->weighted(false);
			}
		}
	}
}

void SoftBodyPlayer::all(bool status)
{
	for (int i = 0; i < springList.size(); i++)
	{
		springList[i]->expand(status);
	}
}

void SoftBodyPlayer::goBoom(PhysicsScene* scene)
{
	for (int i = 0; i < springList.size(); i++)
	{
		if (springList[i] != nullptr)
		{
			scene->removeActor(springList[i]);
		}
	}

	for (int i = 0; i < sphereList.size(); i++)
	{
		if (sphereList[i] != nullptr)
		{
			sphereList[i]->setShapeID(COSMETIC_SPHERE);
		}
	}
	isDead = true;
}


