#pragma once
#include <vector>
#include "Spring.h"
#include "PhysicsScene.h"

class Sphere;

class SoftBodyPlayer
{
public:
	SoftBodyPlayer(PhysicsScene* scene, int layers, float distanceApart, float strength, glm::vec2 position, float expansion, float ballSize, bool isAI, SoftBodyAI* parent = nullptr);
	~SoftBodyPlayer();

	void createShape(PhysicsScene* scene, std::vector<std::string>& text, glm::vec2 position, float distanceApart, float strength, float ballSize);

	void squareConnect(PhysicsScene* scene, Sphere* sphere00, Sphere* sphere10, Sphere* sphere01, Sphere* sphere11, float distanceApart, float strength);
	void noRoofConnect(PhysicsScene* scene, Sphere* sphere00, Sphere* sphere10, Sphere* sphere01, Sphere* sphere11, float distanceApart, float strength);
	void noLeftConnect(PhysicsScene* scene, Sphere* sphere00, Sphere* sphere10, Sphere* sphere01, Sphere* sphere11, float distanceApart, float strength);
	void cornerConnect(PhysicsScene* scene, Sphere* sphere00, Sphere* sphere10, Sphere* sphere01, Sphere* sphere11, float distanceApart, float strength);
	
	glm::vec2 getPosition();

	float getCamX();
	float getCamY();


	void right(bool status);
	void left(bool status);
	void down(bool status);
	void up(bool status);
	void all(bool status);

	void goBoom(PhysicsScene* scene);

	bool isAlive() { return !isDead; }
	

protected:
	std::vector<Spring*> springList;
	std::vector<Sphere*> sphereList;

	float globalDampness = 10;

	glm::vec2 m_center;

	float m_expansion;
	float m_distanceApart;

	int m_windowWidth = 1280;
	int m_windowHeight = 720;
	bool isDead = false;
};

