#include "Sphere.h"
#include "SoftBodyAI.h"




Sphere::~Sphere()
{
}

void Sphere::makeGizmo()
{
	aie::Gizmos::add2DCircle(m_position, m_radius, 16, m_colour, nullptr);
}

void Sphere::massChange(float massDifference)
{
	m_mass += massDifference;
}

void Sphere::weighted(bool isWeighted)
{
	if (isWeighted)
	{
		if (m_weighted)
		{
			return;
		}
		else
		{
			m_mass = m_initialMass * 5.0f;
			m_weighted = true;
		}
	}
	else
	{
		if(m_weighted)
		{
			m_mass = m_initialMass;
			m_weighted = false;
		}
		else
		{
			return;
		}
	}
}

void Sphere::setParent(SoftBodyAI * parent, PhysicsScene * scene)
{
	m_possibleParent = parent;
	m_sceneReference = scene;
}

void Sphere::specialCollision()
{
	if (m_possibleParent != nullptr)
	{
		m_possibleParent->goBoom(m_sceneReference);
	}
}
