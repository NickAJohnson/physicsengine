#pragma once
#include "RigidBody.h"
#include "Gizmos.h"

class PhysicsScene;
class SoftBodyAI;
class Sphere :
	public RigidBody
{
public:
	Sphere(glm::vec2 position, glm::vec2 velocity, float mass, float radius, glm::vec4 colour) :
		RigidBody(SPHERE, position, velocity, 0, mass)
	{
		m_radius = radius;
		m_colour = colour;
		m_moment = 0.5f * m_mass * m_radius * m_radius;
		m_initialMass = mass;
	}
	~Sphere();

	virtual void makeGizmo();

	void massChange(float massDifference);
	void weighted(bool isWeighted);
	float getRadius() { return m_radius; }
	glm::vec4 getColour() { return m_colour; }
	void setParent(SoftBodyAI* parent, PhysicsScene* scene);
	void specialCollision();

protected:
	float m_radius;
	bool m_weighted = false;
	float m_initialMass;
	glm::vec4 m_colour;

	//Used to call self destruct from sphere collision
	SoftBodyAI* m_possibleParent = nullptr;
	PhysicsScene* m_sceneReference = nullptr;
};

