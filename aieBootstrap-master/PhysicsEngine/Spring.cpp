#include "Spring.h"

Spring::~Spring()
{
}

void Spring::fixedUpdate(glm::vec2 gravity, float timeStep)
{
	glm::vec2 p2 = m_body2->toWorld(m_contact2);
	glm::vec2 p1 = m_body1->toWorld(m_contact1);
	glm::vec2 dist = p2 - p1;
	float length = sqrtf(dist.x*dist.x + dist.y*dist.y);

	//apply damping
	glm::vec2 relativeVelocity = m_body2->getVelocity() - m_body1->getVelocity();
	//F = -kX - bV
	glm::vec2 force = dist * m_springCoefficient * (m_restLength - length) - m_damping * relativeVelocity;

	m_body1->applyForce(-force * timeStep, p1 - m_body1->getPosition());
	m_body2->applyForce(force * timeStep, p2 - m_body2->getPosition());
}

void Spring::makeGizmo()
{
	glm::vec4 colour(1, 1, 1, 1);
	aie::Gizmos::add2DLine(m_body1->toWorld(m_contact1), m_body2->toWorld(m_contact2), colour);
}

void Spring::isThisMikesHair(bool yeahnah)
{
	if (yeahnah)
	{
		m_damping = -.5f;
	}
	else
	{
		m_damping = .1f;
	}
}

glm::vec2 Spring::getCenter()
{
	return glm::vec2((m_body1->getPosition().x + m_body2->getPosition().x) / 2, (m_body1->getPosition().y + m_body2->getPosition().y) / 2);
}

void Spring::expand(bool status)
{
	if (status)
	{
		if (expanded)
		{
			return;
		}
		else
		{
			expanded = true;
			m_restLength = 2 * m_initialRestLength;
		}
	}
	else
	{
		if (expanded)
		{
			expanded = false;
			m_restLength = m_initialRestLength;
		}
		else
		{
			return;
		}
	}
}


