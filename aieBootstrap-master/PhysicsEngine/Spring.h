#pragma once
#include "PhysicsObject.h"

#include <glm/glm.hpp>
#include "Gizmos.h"
#include "Box.h"
#include "RigidBody.h"
class Spring :
	public PhysicsObject
{
public:
	Spring(RigidBody* body1, RigidBody* body2, float restLength, float springCoefficient, 
		float damping = 0.1f, glm::vec2 contact1 = glm::vec2(0,0), glm::vec2 contact2 = glm::vec2(0,0)) : PhysicsObject(JOINT)
	{
		m_body1 = body1;
		m_body2 = body2;
		m_restLength = restLength;
		m_initialRestLength = restLength;
		m_springCoefficient = springCoefficient;
		m_damping = damping;
		m_contact1 = contact1;
		m_contact2 = contact2;
	}
	~Spring();

	virtual void debug() {};

	void fixedUpdate(glm::vec2 gravity, float timeStep);
	void makeGizmo();
	void isThisMikesHair(bool yeahnah);
	void setRestLength(float restLength) { m_restLength = restLength; }

	float getRestLength() { return m_restLength; }

	glm::vec2 getCenter();

	void expand(bool status);


private:
	bool expanded = false;
	float m_expand = 2.0f;
	RigidBody* m_body1;
	RigidBody* m_body2;

	glm::vec2 m_contact1;
	glm::vec2 m_contact2;

	float m_damping;
	float m_restLength;
	float m_initialRestLength;
	float m_springCoefficient; // Restoring force 
};

