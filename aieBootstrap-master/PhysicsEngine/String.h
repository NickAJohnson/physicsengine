#pragma once
#include "Spring.h"
class String :
	public Spring
{
public:
	String(RigidBody* body1, RigidBody* body2, float restLength, float springCoefficient,
		float damping = 0.1f, glm::vec2 contact1 = glm::vec2(0, 0), glm::vec2 contact2 = glm::vec2(0, 0)) : Spring(body1, body2, restLength, springCoefficient)
	{
		m_body1 = body1;
		m_body2 = body2;
		m_restLength = restLength;
		m_springCoefficient = springCoefficient;
		m_damping = damping;
		m_contact1 = contact1;
		m_contact2 = contact2;
	}
	~String();

	virtual void debug() {};

	void fixedUpdate(glm::vec2 gravity, float timeStep);

private:
	RigidBody* m_body1;
	RigidBody* m_body2;

	glm::vec2 m_contact1;
	glm::vec2 m_contact2;

	float m_damping;
	float m_restLength;
	float m_springCoefficient; // Restoring force 
};


